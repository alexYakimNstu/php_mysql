<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01.03.18
 * Time: 21:18
 */

namespace TestTask;


class TripleSeventeens
{
    public function __construct()
    {
    }

    /**
     * Return numbers triple less then 17
     *
     * @return array
     */
    public function getNumbersTripleSeventeens()
    {
        $maxNumber = 1000;
        $result = [];

        for ($i=1; $i <= $maxNumber;  $i++) {

            $digits = preg_split('//', $i, -1, PREG_SPLIT_NO_EMPTY);

            if ($i % 3 == 0 && array_sum($digits)) {
                $result[] = $i;
            }
        }

        return $result;
    }

}
