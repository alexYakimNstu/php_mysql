<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01.03.18
 * Time: 20:32
 */

namespace TestTask;

class TestTask
{
    public function __construct()
    {
        $this->render();
    }

    /**
     * Show results of tasks
     */
    public function render()
    {
        require 'TaskFirst.php';
        $firstTask = new TaskFirst();
        $firstTaskResult = $firstTask->getResultFirstTask();
        echo('TaskFirst: ' . implode(', ', $firstTaskResult) . '\n');

        require 'Palindrome.php';
        $seckondTask = new Palindrome();

        $arrayString = [
            'racecar', 'a man a plan a canal Panama.', 'R2D2',
            'Desserts, I stressed!', 'Red rum, sir, is murder.', 'This is the final one'
        ];

        var_dump('seckondTask');
        foreach ($arrayString as $string) {
            var_dump($seckondTask->isPalindromeString($string));
        }

        require 'TripleSeventeens.php';
        $thirdTask = new TripleSeventeens();
        $thirdTaskResult = $thirdTask->getNumbersTripleSeventeens();

        var_dump('thirdTaskResult', implode(',', $thirdTaskResult));

    }
}