<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01.03.18
 * Time: 20:53
 */

namespace TestTask;

class Palindrome
{
    public function __construct()
    {

    }

    /**
     * Check for palindrome
     *
     * @param $string
     * @return bool|string
     */
    public function isPalindromeString($string)
    {
        if (empty($string)) {
            return 'Error with input';
        }


        $string = strtolower(preg_replace ("/[^a-zA-Z0-9]/", "", $string));

        return $string == strrev($string);
    }
}
