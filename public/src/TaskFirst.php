<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01.03.18
 * Time: 20:27
 */

namespace TestTask;

class TaskFirst
{

    public function __construct()
    {
        
    }

    /**
     * Return array divisible exactly
     *
     * @return array
     */
    public function getResultFirstTask()
    {
        $maxNumber = 100;
        $result = [];

        for ($i=1; $i <= $maxNumber;  $i++) {
            if ($i % 3 == 0 && $i %4 == 0) {
                $result[] = 'Hanunga';
            } elseif ($i % 3 == 0) {
                $result[] = 'Nureek';
            } elseif ($i % 4 == 0) {
                $result[] = 'Retut';
            } else {
                $result[] = $i;
            }
        }

        return $result;
    }
    
}